# oVirt Desktop Client

### 描述

oVirt-desktop-client是一个用pyQt开发的简易oVirt桌面客户端，能够列出用户虚机列表，通过调用remote-viewer访问虚机。

### 安装 && 配置 && 运行

1.首先你需要一个centos7的GUI环境   //原则上其它linux系统也可，我没做测试

2.安装依赖

yum -y install python36 python36-devel python36-virtualenv python36-pip gcc gcc-c++ qt5-qtbase qt5-qtbase-devel libcurl-devel libxml2 libxml2-devel libxslt-devel openssl-devel virt-viewer

3.克隆源码

git clone https://gitee.com/cnovirt/ovirt-desktop-client.git

4.准备python环境

virtualenv-3 venv

. venv/bin/activate

pip install --upgrade pip -i https://pypi.mirrors.ustc.edu.cn/simple/

5.安装python依赖

cd ovirt-desktop-client

pull

pip install -r requirements.txt -i https://pypi.mirrors.ustc.edu.cn/simple/

6.下载证书

从https://engine230.cloud/ovirt-engine/ 页面，下载CA证书    //注意这里的"engine230.cloud"根据你的环境填写

将下载下来的pki-resource.cer重命名为ca.crt，放到root目录下

7.修改配置文件

cp settings.conf.example settings.conf

vi settings.conf

修改以下三项：

url = https://engine230.cloud/ovirt-engine/api     //注意这里的"engine230.cloud"根据你的环境填写

cafile = /root/ca.crt

domain = internal

8.运行客户端程序

python ovirtclient.py

![ovirt-desktop-client](https://images.gitee.com/uploads/images/2020/0403/172703_9c297e7c_5734030.png "ovirt-desktop-client")
